-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.25-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for essentialmode
CREATE DATABASE IF NOT EXISTS `essentialmode` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `essentialmode`;

-- Dumping structure for table essentialmode.addon_account
CREATE TABLE IF NOT EXISTS `addon_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.addon_account: ~6 rows (approximately)
DELETE FROM `addon_account`;
/*!40000 ALTER TABLE `addon_account` DISABLE KEYS */;
INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
	(1, 'society_ambulance', 'Ambulance', 1),
	(2, 'society_police', 'Police', 1),
	(3, 'society_police', 'Police', 1),
	(4, 'caution', 'Caution', 0),
	(5, 'society_mecano', 'Mécano', 1),
	(6, 'society_cardealer', 'Concessionnaire', 1);
/*!40000 ALTER TABLE `addon_account` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_account_data
CREATE TABLE IF NOT EXISTS `addon_account_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.addon_account_data: ~5 rows (approximately)
DELETE FROM `addon_account_data`;
/*!40000 ALTER TABLE `addon_account_data` DISABLE KEYS */;
INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
	(1, 'society_ambulance', 0, NULL),
	(2, 'society_police', 0, NULL),
	(3, 'caution', 0, 'steam:1100001017f3590'),
	(4, 'society_mecano', 0, NULL),
	(5, 'society_cardealer', 0, NULL);
/*!40000 ALTER TABLE `addon_account_data` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_inventory
CREATE TABLE IF NOT EXISTS `addon_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.addon_inventory: ~3 rows (approximately)
DELETE FROM `addon_inventory`;
/*!40000 ALTER TABLE `addon_inventory` DISABLE KEYS */;
INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
	(1, 'society_police', 'Police', 1),
	(2, 'society_mecano', 'Mécano', 1),
	(3, 'society_cardealer', 'Concesionnaire', 1);
/*!40000 ALTER TABLE `addon_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_inventory_items
CREATE TABLE IF NOT EXISTS `addon_inventory_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.addon_inventory_items: ~0 rows (approximately)
DELETE FROM `addon_inventory_items`;
/*!40000 ALTER TABLE `addon_inventory_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `addon_inventory_items` ENABLE KEYS */;

-- Dumping structure for table essentialmode.billing
CREATE TABLE IF NOT EXISTS `billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `target_type` varchar(50) NOT NULL,
  `target` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.billing: ~0 rows (approximately)
DELETE FROM `billing`;
/*!40000 ALTER TABLE `billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing` ENABLE KEYS */;

-- Dumping structure for table essentialmode.cardealer_vehicles
CREATE TABLE IF NOT EXISTS `cardealer_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.cardealer_vehicles: ~0 rows (approximately)
DELETE FROM `cardealer_vehicles`;
/*!40000 ALTER TABLE `cardealer_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `cardealer_vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.characters
CREATE TABLE IF NOT EXISTS `characters` (
  `identifier` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofbirth` varchar(255) NOT NULL,
  `sex` varchar(1) NOT NULL DEFAULT 'f',
  `height` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.characters: ~1 rows (approximately)
DELETE FROM `characters`;
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` (`identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`) VALUES
	('steam:1100001017f3590', 'Glen', 'Gustavo', '04/23/1991', 'M', '80');
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;

-- Dumping structure for table essentialmode.datastore
CREATE TABLE IF NOT EXISTS `datastore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.datastore: ~2 rows (approximately)
DELETE FROM `datastore`;
/*!40000 ALTER TABLE `datastore` DISABLE KEYS */;
INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
	(1, 'society_police', 'Police', 1),
	(2, 'society_police', 'Police', 1);
/*!40000 ALTER TABLE `datastore` ENABLE KEYS */;

-- Dumping structure for table essentialmode.datastore_data
CREATE TABLE IF NOT EXISTS `datastore_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `data` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.datastore_data: ~1 rows (approximately)
DELETE FROM `datastore_data`;
/*!40000 ALTER TABLE `datastore_data` DISABLE KEYS */;
INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
	(1, 'society_police', NULL, '{}');
/*!40000 ALTER TABLE `datastore_data` ENABLE KEYS */;

-- Dumping structure for table essentialmode.fine_types
CREATE TABLE IF NOT EXISTS `fine_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.fine_types: ~52 rows (approximately)
DELETE FROM `fine_types`;
/*!40000 ALTER TABLE `fine_types` DISABLE KEYS */;
INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
	(1, 'Usage abusif du klaxon', 30, 0),
	(2, 'Franchir une ligne continue', 40, 0),
	(3, 'Circulation à contresens', 250, 0),
	(4, 'Demi-tour non autorisé', 250, 0),
	(5, 'Circulation hors-route', 170, 0),
	(6, 'Non-respect des distances de sécurité', 30, 0),
	(7, 'Arrêt dangereux / interdit', 150, 0),
	(8, 'Stationnement gênant / interdit', 70, 0),
	(9, 'Non respect  de la priorité à droite', 70, 0),
	(10, 'Non-respect à un véhicule prioritaire', 90, 0),
	(11, 'Non-respect d\'un stop', 105, 0),
	(12, 'Non-respect d\'un feu rouge', 130, 0),
	(13, 'Dépassement dangereux', 100, 0),
	(14, 'Véhicule non en état', 100, 0),
	(15, 'Conduite sans permis', 1500, 0),
	(16, 'Délit de fuite', 800, 0),
	(17, 'Excès de vitesse < 5 kmh', 90, 0),
	(18, 'Excès de vitesse 5-15 kmh', 120, 0),
	(19, 'Excès de vitesse 15-30 kmh', 180, 0),
	(20, 'Excès de vitesse > 30 kmh', 300, 0),
	(21, 'Entrave de la circulation', 110, 1),
	(22, 'Dégradation de la voie publique', 90, 1),
	(23, 'Trouble à l\'ordre publique', 90, 1),
	(24, 'Entrave opération de police', 130, 1),
	(25, 'Insulte envers / entre civils', 75, 1),
	(26, 'Outrage à agent de police', 110, 1),
	(27, 'Menace verbale ou intimidation envers civil', 90, 1),
	(28, 'Menace verbale ou intimidation envers policier', 150, 1),
	(29, 'Manifestation illégale', 250, 1),
	(30, 'Tentative de corruption', 1500, 1),
	(31, 'Arme blanche sortie en ville', 120, 2),
	(32, 'Arme léthale sortie en ville', 300, 2),
	(33, 'Port d\'arme non autorisé (défaut de license)', 600, 2),
	(34, 'Port d\'arme illégal', 700, 2),
	(35, 'Pris en flag lockpick', 300, 2),
	(36, 'Vol de voiture', 1800, 2),
	(37, 'Vente de drogue', 1500, 2),
	(38, 'Fabriquation de drogue', 1500, 2),
	(39, 'Possession de drogue', 650, 2),
	(40, 'Prise d\'ôtage civil', 1500, 2),
	(41, 'Prise d\'ôtage agent de l\'état', 2000, 2),
	(42, 'Braquage particulier', 650, 2),
	(43, 'Braquage magasin', 650, 2),
	(44, 'Braquage de banque', 1500, 2),
	(45, 'Tir sur civil', 2000, 3),
	(46, 'Tir sur agent de l\'état', 2500, 3),
	(47, 'Tentative de meurtre sur civil', 3000, 3),
	(48, 'Tentative de meurtre sur agent de l\'état', 5000, 3),
	(49, 'Meurtre sur civil', 10000, 3),
	(50, 'Meurte sur agent de l\'état', 30000, 3),
	(51, 'Meurtre involontaire', 1800, 3),
	(52, 'Escroquerie à l\'entreprise', 2000, 2);
/*!40000 ALTER TABLE `fine_types` ENABLE KEYS */;

-- Dumping structure for table essentialmode.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `limit` int(11) NOT NULL DEFAULT '-1',
  `rare` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.items: ~35 rows (approximately)
DELETE FROM `items`;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`) VALUES
	(1, 'bandage', 'Bandage', -1, 0, 1),
	(2, 'medikit', 'Medikit', -1, 0, 1),
	(3, 'alive_chicken', 'Alive Chicken', -1, 0, 1),
	(4, 'slaughtered_chicken', 'Dead Chicken', -1, 0, 1),
	(5, 'packaged_chicken', 'Packaged Chicken', -1, 0, 1),
	(6, 'fish', 'Fish', -1, 0, 1),
	(7, 'stone', 'Stone', -1, 0, 1),
	(8, 'washed_stone', 'Washed Stone', -1, 0, 1),
	(9, 'copper', 'Copper', -1, 0, 1),
	(10, 'iron', 'Iron', -1, 0, 1),
	(11, 'gold', 'Gold', -1, 0, 1),
	(12, 'diamond', 'Diamond', -1, 0, 1),
	(13, 'wood', 'Wood', -1, 0, 1),
	(14, 'cutted_wood', 'Chopped Wood', -1, 0, 1),
	(15, 'packaged_plank', 'Packaged Plank', -1, 0, 1),
	(16, 'petrol', 'Petrol', -1, 0, 1),
	(17, 'petrol_raffin', 'Clean Petrol', -1, 0, 1),
	(18, 'essence', 'Essence', -1, 0, 1),
	(19, 'whool', 'Wool', -1, 0, 1),
	(20, 'fabric', 'Fabric', -1, 0, 1),
	(21, 'clothe', 'Clothes', -1, 0, 1),
	(22, 'gazbottle', 'Gas Bottle', -1, 0, 1),
	(23, 'fixtool', 'Repair Tool', -1, 0, 1),
	(24, 'carotool', 'Car Tool', -1, 0, 1),
	(25, 'blowpipe', 'Blow Pipe', -1, 0, 1),
	(26, 'fixkit', 'Fix Kit', -1, 0, 1),
	(27, 'carokit', 'Car Kit', -1, 0, 1),
	(28, 'weed', 'Weed', -1, 0, 1),
	(29, 'weed_pooch', 'Weed Pouch', -1, 0, 1),
	(30, 'coke', 'Coke', -1, 0, 1),
	(31, 'coke_pooch', 'Coke Bag', -1, 0, 1),
	(32, 'meth', 'Meth', -1, 0, 1),
	(33, 'meth_pooch', 'Meth Bag', -1, 0, 1),
	(34, 'opium', 'Opium', -1, 0, 1),
	(35, 'opium_pooch', 'Opium Pouch', -1, 0, 1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.jobs: ~12 rows (approximately)
DELETE FROM `jobs`;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
	(1, 'unemployed', 'unemployed', 0),
	(2, 'ambulance', 'Ambulance', 1),
	(3, 'police', 'LSPD', 1),
	(4, 'slaughterer', 'Hunting', 0),
	(5, 'fisherman', 'Fisherman', 0),
	(6, 'miner', 'Miner', 0),
	(7, 'lumberjack', 'Lumberjack', 0),
	(8, 'fuel', 'P.S Oil', 0),
	(9, 'reporter', 'Reporter', 0),
	(10, 'textil', 'Fashion Designer', 0),
	(11, 'mecano', 'P.S Mechanics', 0),
	(12, 'cardealer', 'Concessionnaire', 1);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table essentialmode.job_grades
CREATE TABLE IF NOT EXISTS `job_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(255) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext NOT NULL,
  `skin_female` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.job_grades: ~26 rows (approximately)
DELETE FROM `job_grades`;
/*!40000 ALTER TABLE `job_grades` DISABLE KEYS */;
INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
	(1, 'unemployed', 0, 'rsa', '', 300, '{}', '{}'),
	(2, 'ambulance', 1, 'ambulance', 'Basic Medic', 600, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(3, 'ambulance', 2, 'doctor', 'Sergeant Medic', 1000, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(4, 'ambulance', 3, 'chief_doctor', 'Staff Sergeant Medic', 1500, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(5, 'ambulance', 4, 'boss', 'Cheif Medic', 2000, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(6, 'police', 1, 'recruit', 'Cadet', 600, '{"tshirt_1":57,"torso_1":55,"arms":0,"pants_1":35,"glasses":0,"decals_2":0,"hair_color_2":0,"helmet_2":0,"hair_color_1":5,"face":19,"glasses_2":1,"torso_2":0,"shoes":24,"hair_1":2,"skin":34,"sex":0,"glasses_1":0,"pants_2":0,"hair_2":0,"decals_1":0,"tshirt_2":0,"helmet_1":8}', '{"tshirt_1":34,"torso_1":48,"shoes":24,"pants_1":34,"torso_2":0,"decals_2":0,"hair_color_2":0,"glasses":0,"helmet_2":0,"hair_2":3,"face":21,"decals_1":0,"glasses_2":1,"hair_1":11,"skin":34,"sex":1,"glasses_1":5,"pants_2":0,"arms":14,"hair_color_1":10,"tshirt_2":0,"helmet_1":57}'),
	(7, 'police', 2, 'officer', 'Officer', 1000, '{"tshirt_1":58,"torso_1":55,"shoes":24,"pants_1":35,"pants_2":0,"decals_2":1,"hair_color_2":0,"face":19,"helmet_2":0,"hair_2":0,"arms":0,"decals_1":8,"torso_2":0,"hair_1":2,"skin":34,"sex":0,"glasses_1":0,"glasses_2":1,"hair_color_1":5,"glasses":0,"tshirt_2":0,"helmet_1":11}', '{"tshirt_1":35,"torso_1":48,"arms":14,"pants_1":34,"pants_2":0,"decals_2":1,"hair_color_2":0,"shoes":24,"helmet_2":0,"hair_2":3,"decals_1":7,"torso_2":0,"face":21,"hair_1":11,"skin":34,"sex":1,"glasses_1":5,"glasses_2":1,"hair_color_1":10,"glasses":0,"tshirt_2":0,"helmet_1":57}'),
	(8, 'police', 3, 'sergeant', 'Sergent', 1500, '{"tshirt_1":58,"torso_1":55,"shoes":24,"pants_1":35,"pants_2":0,"decals_2":1,"hair_color_2":0,"face":19,"helmet_2":0,"hair_2":0,"arms":0,"decals_1":8,"torso_2":0,"hair_1":2,"skin":34,"sex":0,"glasses_1":0,"glasses_2":1,"hair_color_1":5,"glasses":0,"tshirt_2":0,"helmet_1":11}', '{"tshirt_1":35,"torso_1":48,"arms":14,"pants_1":34,"pants_2":0,"decals_2":1,"hair_color_2":0,"shoes":24,"helmet_2":0,"hair_2":3,"decals_1":7,"torso_2":0,"face":21,"hair_1":11,"skin":34,"sex":1,"glasses_1":5,"glasses_2":1,"hair_color_1":10,"glasses":0,"tshirt_2":0,"helmet_1":57}'),
	(9, 'police', 4, 'lieutenant', 'Lieutenant', 2000, '{"tshirt_1":58,"torso_1":55,"shoes":24,"pants_1":35,"pants_2":0,"decals_2":2,"hair_color_2":0,"face":19,"helmet_2":0,"hair_2":0,"glasses":0,"decals_1":8,"hair_color_1":5,"hair_1":2,"skin":34,"sex":0,"glasses_1":0,"glasses_2":1,"torso_2":0,"arms":41,"tshirt_2":0,"helmet_1":11}', '{"tshirt_1":35,"torso_1":48,"arms":44,"pants_1":34,"hair_2":3,"decals_2":2,"hair_color_2":0,"hair_color_1":10,"helmet_2":0,"face":21,"shoes":24,"torso_2":0,"glasses_2":1,"hair_1":11,"skin":34,"sex":1,"glasses_1":5,"pants_2":0,"decals_1":7,"glasses":0,"tshirt_2":0,"helmet_1":57}'),
	(10, 'police', 5, 'boss', 'Cheif', 2500, '{"tshirt_1":58,"torso_1":55,"shoes":24,"pants_1":35,"pants_2":0,"decals_2":3,"hair_color_2":0,"face":19,"helmet_2":0,"hair_2":0,"arms":41,"torso_2":0,"hair_color_1":5,"hair_1":2,"skin":34,"sex":0,"glasses_1":0,"glasses_2":1,"decals_1":8,"glasses":0,"tshirt_2":0,"helmet_1":11}', '{"tshirt_1":35,"torso_1":48,"arms":44,"pants_1":34,"pants_2":0,"decals_2":3,"hair_color_2":0,"face":21,"helmet_2":0,"hair_2":3,"decals_1":7,"torso_2":0,"hair_color_1":10,"hair_1":11,"skin":34,"sex":1,"glasses_1":5,"glasses_2":1,"shoes":24,"glasses":0,"tshirt_2":0,"helmet_1":57}'),
	(11, 'lumberjack', 0, 'interim', '', 50, '{}', '{}'),
	(12, 'fisherman', 0, 'interim', '', 50, '{}', '{}'),
	(13, 'fuel', 0, 'interim', '', 50, '{}', '{}'),
	(14, 'reporter', 0, 'employee', '', 50, '{}', '{}'),
	(15, 'textil', 0, 'interim', '', 50, '{"mask_1":0,"arms":1,"glasses_1":0,"hair_color_2":4,"makeup_1":0,"face":19,"glasses":0,"mask_2":0,"makeup_3":0,"skin":29,"helmet_2":0,"lipstick_4":0,"sex":0,"torso_1":24,"makeup_2":0,"bags_2":0,"chain_2":0,"ears_1":-1,"bags_1":0,"bproof_1":0,"shoes_2":0,"lipstick_2":0,"chain_1":0,"tshirt_1":0,"eyebrows_3":0,"pants_2":0,"beard_4":0,"torso_2":0,"beard_2":6,"ears_2":0,"hair_2":0,"shoes_1":36,"tshirt_2":0,"beard_3":0,"hair_1":2,"hair_color_1":0,"pants_1":48,"helmet_1":-1,"bproof_2":0,"eyebrows_4":0,"eyebrows_2":0,"decals_1":0,"age_2":0,"beard_1":5,"shoes":10,"lipstick_1":0,"eyebrows_1":0,"glasses_2":0,"makeup_4":0,"decals_2":0,"lipstick_3":0,"age_1":0}', '{"mask_1":0,"arms":5,"glasses_1":5,"hair_color_2":4,"makeup_1":0,"face":19,"glasses":0,"mask_2":0,"makeup_3":0,"skin":29,"helmet_2":0,"lipstick_4":0,"sex":1,"torso_1":52,"makeup_2":0,"bags_2":0,"chain_2":0,"ears_1":-1,"bags_1":0,"bproof_1":0,"shoes_2":1,"lipstick_2":0,"chain_1":0,"tshirt_1":23,"eyebrows_3":0,"pants_2":0,"beard_4":0,"torso_2":0,"beard_2":6,"ears_2":0,"hair_2":0,"shoes_1":42,"tshirt_2":4,"beard_3":0,"hair_1":2,"hair_color_1":0,"pants_1":36,"helmet_1":-1,"bproof_2":0,"eyebrows_4":0,"eyebrows_2":0,"decals_1":0,"age_2":0,"beard_1":5,"shoes":10,"lipstick_1":0,"eyebrows_1":0,"glasses_2":0,"makeup_4":0,"decals_2":0,"lipstick_3":0,"age_1":0}'),
	(16, 'miner', 0, 'interim', '', 50, '{"tshirt_2":1,"ears_1":8,"glasses_1":15,"torso_2":0,"ears_2":2,"glasses_2":3,"shoes_2":1,"pants_1":75,"shoes_1":51,"bags_1":0,"helmet_2":0,"pants_2":7,"torso_1":71,"tshirt_1":59,"arms":2,"bags_2":0,"helmet_1":0}', '{}'),
	(17, 'slaughterer', 0, 'interim', '', 50, '{"age_1":0,"glasses_2":0,"beard_1":5,"decals_2":0,"beard_4":0,"shoes_2":0,"tshirt_2":0,"lipstick_2":0,"hair_2":0,"arms":67,"pants_1":36,"skin":29,"eyebrows_2":0,"shoes":10,"helmet_1":-1,"lipstick_1":0,"helmet_2":0,"hair_color_1":0,"glasses":0,"makeup_4":0,"makeup_1":0,"hair_1":2,"bproof_1":0,"bags_1":0,"mask_1":0,"lipstick_3":0,"chain_1":0,"eyebrows_4":0,"sex":0,"torso_1":56,"beard_2":6,"shoes_1":12,"decals_1":0,"face":19,"lipstick_4":0,"tshirt_1":15,"mask_2":0,"age_2":0,"eyebrows_3":0,"chain_2":0,"glasses_1":0,"ears_1":-1,"bags_2":0,"ears_2":0,"torso_2":0,"bproof_2":0,"makeup_2":0,"eyebrows_1":0,"makeup_3":0,"pants_2":0,"beard_3":0,"hair_color_2":4}', '{"age_1":0,"glasses_2":0,"beard_1":5,"decals_2":0,"beard_4":0,"shoes_2":0,"tshirt_2":0,"lipstick_2":0,"hair_2":0,"arms":72,"pants_1":45,"skin":29,"eyebrows_2":0,"shoes":10,"helmet_1":-1,"lipstick_1":0,"helmet_2":0,"hair_color_1":0,"glasses":0,"makeup_4":0,"makeup_1":0,"hair_1":2,"bproof_1":0,"bags_1":0,"mask_1":0,"lipstick_3":0,"chain_1":0,"eyebrows_4":0,"sex":1,"torso_1":49,"beard_2":6,"shoes_1":24,"decals_1":0,"face":19,"lipstick_4":0,"tshirt_1":9,"mask_2":0,"age_2":0,"eyebrows_3":0,"chain_2":0,"glasses_1":5,"ears_1":-1,"bags_2":0,"ears_2":0,"torso_2":0,"bproof_2":0,"makeup_2":0,"eyebrows_1":0,"makeup_3":0,"pants_2":0,"beard_3":0,"hair_color_2":4}'),
	(18, 'mecano', 0, 'recrue', '', 12, '{}', '{}'),
	(19, 'mecano', 1, 'novice', 'Novice', 24, '{}', '{}'),
	(20, 'mecano', 2, 'experimente', 'Experimente', 36, '{}', '{}'),
	(21, 'mecano', 3, 'chief', 'Chef d\'équipe', 48, '{}', '{}'),
	(22, 'mecano', 4, 'boss', 'Patron', 0, '{}', '{}'),
	(23, 'cardealer', 0, 'recruit', 'Recrue', 10, '{}', '{}'),
	(24, 'cardealer', 1, 'novice', 'Novice', 25, '{}', '{}'),
	(25, 'cardealer', 2, 'experienced', 'Experimente', 40, '{}', '{}'),
	(26, 'cardealer', 3, 'boss', 'Patron', 0, '{}', '{}');
/*!40000 ALTER TABLE `job_grades` ENABLE KEYS */;

-- Dumping structure for table essentialmode.licenses
CREATE TABLE IF NOT EXISTS `licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.licenses: ~1 rows (approximately)
DELETE FROM `licenses`;
/*!40000 ALTER TABLE `licenses` DISABLE KEYS */;
INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
	(1, 'weapon', 'Permis de port d\'arme');
/*!40000 ALTER TABLE `licenses` ENABLE KEYS */;

-- Dumping structure for table essentialmode.owned_vehicles
CREATE TABLE IF NOT EXISTS `owned_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.owned_vehicles: ~1 rows (approximately)
DELETE FROM `owned_vehicles`;
/*!40000 ALTER TABLE `owned_vehicles` DISABLE KEYS */;
INSERT INTO `owned_vehicles` (`id`, `vehicle`, `owner`) VALUES
	(8, '{"modTransmission":2,"modSmokeEnabled":1,"modBackWheels":-1,"modHydrolic":-1,"modGrille":-1,"modHorns":-1,"modLivery":-1,"modEngine":3,"modEngineBlock":-1,"modTurbo":false,"modSteeringWheel":-1,"modAerials":-1,"modArmor":-1,"modDoorSpeaker":-1,"modBrakes":2,"modHood":-1,"neonColor":[255,0,255],"dirtLevel":3.5504779815674,"modShifterLeavers":-1,"wheels":7,"modXenon":false,"plate":"44OTN125","modTrunk":-1,"modAPlate":-1,"health":996,"modRoof":-1,"neonEnabled":[false,false,false,false],"color2":113,"modTrimA":-1,"modWindows":-1,"modSuspension":3,"modFrontWheels":-1,"modVanityPlate":-1,"windowTint":1,"modExhaust":-1,"plateIndex":0,"modSeats":-1,"modFrame":-1,"modTrimB":-1,"modAirFilter":-1,"modOrnaments":-1,"modPlateHolder":-1,"wheelColor":156,"modSpeakers":-1,"tyreSmokeColor":[255,255,255],"model":-2030171296,"modRightFender":-1,"modDial":-1,"modSideSkirt":-1,"color1":76,"modFrontBumper":-1,"modTank":-1,"modDashboard":-1,"modSpoilers":-1,"modArchCover":-1,"modFender":-1,"modRearBumper":-1,"modStruts":-1,"pearlescentColor":156}', 'steam:1100001017f3590');
/*!40000 ALTER TABLE `owned_vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.shops
CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.shops: ~6 rows (approximately)
DELETE FROM `shops`;
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;
INSERT INTO `shops` (`id`, `name`, `item`, `price`) VALUES
	(1, 'TwentyFourSeven', 'bread', 30),
	(2, 'TwentyFourSeven', 'water', 15),
	(3, 'RobsLiquor', 'bread', 30),
	(4, 'RobsLiquor', 'water', 15),
	(5, 'LTDgasoline', 'bread', 30),
	(6, 'LTDgasoline', 'water', 15);
/*!40000 ALTER TABLE `shops` ENABLE KEYS */;

-- Dumping structure for table essentialmode.society_moneywash
CREATE TABLE IF NOT EXISTS `society_moneywash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.society_moneywash: ~0 rows (approximately)
DELETE FROM `society_moneywash`;
/*!40000 ALTER TABLE `society_moneywash` DISABLE KEYS */;
/*!40000 ALTER TABLE `society_moneywash` ENABLE KEYS */;

-- Dumping structure for table essentialmode.users
CREATE TABLE IF NOT EXISTS `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin,
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT '0',
  `loadout` longtext COLLATE utf8mb4_bin,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `phone_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `phone_number`) VALUES
	('steam:1100001017f3590', 'license:7dfcc0f2547c8d9e24a7cc7ff94b30c4a866cbee', 755091, 'Puresaw', '{"chain_2":0,"chain_1":0,"skin":0,"sex":0,"pants_1":25,"bproof_1":0,"eyebrows_3":0,"bproof_2":0,"decals_2":3,"makeup_3":0,"makeup_4":0,"hair_1":0,"makeup_2":0,"age_2":0,"beard_3":0,"mask_1":0,"tshirt_1":59,"face":0,"eyebrows_1":0,"glasses_1":0,"arms":41,"shoes_2":0,"eyebrows_2":0,"hair_color_2":0,"ears_1":2,"beard_2":0,"beard_4":0,"hair_2":0,"lipstick_4":0,"age_1":0,"glasses_2":0,"ears_2":0,"lipstick_1":0,"pants_2":0,"eyebrows_4":0,"torso_2":0,"lipstick_2":0,"helmet_2":0,"mask_2":0,"lipstick_3":0,"hair_color_1":0,"beard_1":0,"decals_1":8,"shoes_1":25,"torso_1":55,"helmet_1":46,"tshirt_2":1,"makeup_1":0,"bags_2":0,"bags_1":0}', 'mecano', 0, '[{"label":"Pistol","name":"WEAPON_PISTOL","ammo":39},{"label":"Micro smg","name":"WEAPON_MICROSMG","ammo":26},{"label":"Pump shotgun","name":"WEAPON_PUMPSHOTGUN","ammo":42},{"label":"Sniper rifle","name":"WEAPON_SNIPERRIFLE","ammo":41},{"label":"Special carbine","name":"WEAPON_SPECIALCARBINE","ammo":17},{"label":"Firework","name":"WEAPON_FIREWORK","ammo":15}]', '{"z":30.655639648438,"y":-783.66693115234,"x":233.66253662109}', 912, 5, 'superadmin', 'Glen', 'Gustavo', '04/23/1991', 'M', '80', 27396);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_accounts
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `money` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.user_accounts: ~1 rows (approximately)
DELETE FROM `user_accounts`;
/*!40000 ALTER TABLE `user_accounts` DISABLE KEYS */;
INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
	(1, 'steam:1100001017f3590', 'black_money', 6208);
/*!40000 ALTER TABLE `user_accounts` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_contacts
CREATE TABLE IF NOT EXISTS `user_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.user_contacts: ~0 rows (approximately)
DELETE FROM `user_contacts`;
/*!40000 ALTER TABLE `user_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_contacts` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_inventory
CREATE TABLE IF NOT EXISTS `user_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.user_inventory: ~35 rows (approximately)
DELETE FROM `user_inventory`;
/*!40000 ALTER TABLE `user_inventory` DISABLE KEYS */;
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
	(1, 'steam:1100001017f3590', 'bandage', 20),
	(2, 'steam:1100001017f3590', 'medikit', 5),
	(3, 'steam:1100001017f3590', 'copper', 0),
	(4, 'steam:1100001017f3590', 'iron', 0),
	(5, 'steam:1100001017f3590', 'cutted_wood', 0),
	(6, 'steam:1100001017f3590', 'washed_stone', 0),
	(7, 'steam:1100001017f3590', 'diamond', 0),
	(8, 'steam:1100001017f3590', 'whool', 0),
	(9, 'steam:1100001017f3590', 'petrol', 0),
	(10, 'steam:1100001017f3590', 'fish', 0),
	(11, 'steam:1100001017f3590', 'slaughtered_chicken', 0),
	(12, 'steam:1100001017f3590', 'stone', 0),
	(13, 'steam:1100001017f3590', 'alive_chicken', 0),
	(14, 'steam:1100001017f3590', 'fabric', 0),
	(15, 'steam:1100001017f3590', 'clothe', 0),
	(16, 'steam:1100001017f3590', 'petrol_raffin', 0),
	(17, 'steam:1100001017f3590', 'packaged_chicken', 0),
	(18, 'steam:1100001017f3590', 'packaged_plank', 0),
	(19, 'steam:1100001017f3590', 'gold', 0),
	(20, 'steam:1100001017f3590', 'wood', 0),
	(21, 'steam:1100001017f3590', 'essence', 0),
	(22, 'steam:1100001017f3590', 'carokit', 0),
	(23, 'steam:1100001017f3590', 'fixtool', 0),
	(24, 'steam:1100001017f3590', 'fixkit', 0),
	(25, 'steam:1100001017f3590', 'carotool', 0),
	(26, 'steam:1100001017f3590', 'gazbottle', 0),
	(27, 'steam:1100001017f3590', 'blowpipe', 0),
	(28, 'steam:1100001017f3590', 'meth', 0),
	(29, 'steam:1100001017f3590', 'opium_pooch', 0),
	(30, 'steam:1100001017f3590', 'weed_pooch', 0),
	(31, 'steam:1100001017f3590', 'coke_pooch', 0),
	(32, 'steam:1100001017f3590', 'opium', 0),
	(33, 'steam:1100001017f3590', 'weed', 0),
	(34, 'steam:1100001017f3590', 'meth_pooch', 0),
	(35, 'steam:1100001017f3590', 'coke', 0);
/*!40000 ALTER TABLE `user_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_licenses
CREATE TABLE IF NOT EXISTS `user_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `owner` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.user_licenses: ~0 rows (approximately)
DELETE FROM `user_licenses`;
/*!40000 ALTER TABLE `user_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_licenses` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_parkings
CREATE TABLE IF NOT EXISTS `user_parkings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin,
  `plate` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.user_parkings: ~1 rows (approximately)
DELETE FROM `user_parkings`;
/*!40000 ALTER TABLE `user_parkings` DISABLE KEYS */;
INSERT INTO `user_parkings` (`id`, `identifier`, `vehicle`, `plate`) VALUES
	(267, 'steam:1100001017f3590', '{"modTransmission":2,"modSmokeEnabled":1,"modBackWheels":-1,"modHydrolic":-1,"modGrille":-1,"modHorns":-1,"modLivery":-1,"modEngine":3,"modEngineBlock":-1,"modTurbo":false,"modSteeringWheel":-1,"modAerials":-1,"modArmor":-1,"modDoorSpeaker":-1,"modBrakes":2,"modHood":-1,"neonColor":[255,0,255],"dirtLevel":3.5504779815674,"modShifterLeavers":-1,"wheels":7,"modXenon":false,"plate":"44OTN125","modTrunk":-1,"modAPlate":-1,"health":996,"modRoof":-1,"neonEnabled":[false,false,false,false],"color2":113,"modTrimA":-1,"modWindows":-1,"modSuspension":3,"modFrontWheels":-1,"modVanityPlate":-1,"windowTint":1,"modExhaust":-1,"plateIndex":0,"modSeats":-1,"modFrame":-1,"modTrimB":-1,"modAirFilter":-1,"modOrnaments":-1,"modPlateHolder":-1,"wheelColor":156,"modSpeakers":-1,"tyreSmokeColor":[255,255,255],"model":-2030171296,"modRightFender":-1,"modDial":-1,"modSideSkirt":-1,"color1":76,"modFrontBumper":-1,"modTank":-1,"modDashboard":-1,"modSpoilers":-1,"modArchCover":-1,"modFender":-1,"modRearBumper":-1,"modStruts":-1,"pearlescentColor":156}', '44OTN125');
/*!40000 ALTER TABLE `user_parkings` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.vehicles: ~211 rows (approximately)
DELETE FROM `vehicles`;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
	(1, 'Blade', 'blade', 15000, 'muscle'),
	(2, 'Buccaneer', 'buccaneer', 18000, 'muscle'),
	(3, 'Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
	(4, 'Chino', 'chino', 15000, 'muscle'),
	(5, 'Chino Luxe', 'chino2', 19000, 'muscle'),
	(6, 'Coquette BlackFin', 'coquette3', 55000, 'muscle'),
	(7, 'Dominator', 'dominator', 35000, 'muscle'),
	(8, 'Dukes', 'dukes', 28000, 'muscle'),
	(9, 'Gauntlet', 'gauntlet', 30000, 'muscle'),
	(10, 'Hotknife', 'hotknife', 125000, 'muscle'),
	(11, 'Faction', 'faction', 20000, 'muscle'),
	(12, 'Faction Rider', 'faction2', 30000, 'muscle'),
	(13, 'Faction XL', 'faction3', 40000, 'muscle'),
	(14, 'Nightshade', 'nightshade', 65000, 'muscle'),
	(15, 'Phoenix', 'phoenix', 12500, 'muscle'),
	(16, 'Picador', 'picador', 18000, 'muscle'),
	(17, 'Sabre Turbo', 'sabregt', 20000, 'muscle'),
	(18, 'Sabre GT', 'sabregt2', 25000, 'muscle'),
	(19, 'Slam Van', 'slamvan3', 11500, 'muscle'),
	(20, 'Tampa', 'tampa', 16000, 'muscle'),
	(21, 'Virgo', 'virgo', 14000, 'muscle'),
	(22, 'Vigero', 'vigero', 12500, 'muscle'),
	(23, 'Voodoo', 'voodoo', 7200, 'muscle'),
	(24, 'Blista', 'blista', 8000, 'compacts'),
	(25, 'Brioso R/A', 'brioso', 18000, 'compacts'),
	(26, 'Issi', 'issi2', 10000, 'compacts'),
	(27, 'Panto', 'panto', 10000, 'compacts'),
	(28, 'Prairie', 'prairie', 12000, 'compacts'),
	(29, 'Bison', 'bison', 45000, 'vans'),
	(30, 'Bobcat XL', 'bobcatxl', 32000, 'vans'),
	(31, 'Burrito', 'burrito3', 19000, 'vans'),
	(32, 'Burrito', 'gburrito2', 29000, 'vans'),
	(33, 'Camper', 'camper', 42000, 'vans'),
	(34, 'Gang Burrito', 'gburrito', 45000, 'vans'),
	(35, 'Journey', 'journey', 6500, 'vans'),
	(36, 'Minivan', 'minivan', 13000, 'vans'),
	(37, 'Moonbeam', 'moonbeam', 18000, 'vans'),
	(38, 'Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
	(39, 'Paradise', 'paradise', 19000, 'vans'),
	(40, 'Rumpo', 'rumpo', 15000, 'vans'),
	(41, 'Rumpo Trail', 'rumpo3', 19500, 'vans'),
	(42, 'Surfer', 'surfer', 12000, 'vans'),
	(43, 'Youga', 'youga', 10800, 'vans'),
	(44, 'Youga Luxuary', 'youga2', 14500, 'vans'),
	(45, 'Asea', 'asea', 5500, 'sedans'),
	(46, 'Cognoscenti', 'cognoscenti', 55000, 'sedans'),
	(47, 'Emperor', 'emperor', 8500, 'sedans'),
	(48, 'Fugitive', 'fugitive', 12000, 'sedans'),
	(49, 'Glendale', 'glendale', 6500, 'sedans'),
	(50, 'Intruder', 'intruder', 7500, 'sedans'),
	(51, 'Premier', 'premier', 8000, 'sedans'),
	(52, 'Primo Custom', 'primo2', 14000, 'sedans'),
	(53, 'Regina', 'regina', 5000, 'sedans'),
	(54, 'Schafter', 'schafter2', 25000, 'sedans'),
	(55, 'Stretch', 'stretch', 90000, 'sedans'),
	(56, 'Super Diamond', 'superd', 130000, 'sedans'),
	(57, 'Tailgater', 'tailgater', 30000, 'sedans'),
	(58, 'Warrener', 'warrener', 4000, 'sedans'),
	(59, 'Washington', 'washington', 9000, 'sedans'),
	(60, 'Baller', 'baller2', 40000, 'suvs'),
	(61, 'Baller Sport', 'baller3', 60000, 'suvs'),
	(62, 'Cavalcade', 'cavalcade2', 55000, 'suvs'),
	(63, 'Contender', 'contender', 70000, 'suvs'),
	(64, 'Dubsta', 'dubsta', 45000, 'suvs'),
	(65, 'Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
	(66, 'Fhantom', 'fq2', 17000, 'suvs'),
	(67, 'Grabger', 'granger', 50000, 'suvs'),
	(68, 'Gresley', 'gresley', 47500, 'suvs'),
	(69, 'Huntley S', 'huntley', 40000, 'suvs'),
	(70, 'Landstalker', 'landstalker', 35000, 'suvs'),
	(71, 'Mesa', 'mesa', 16000, 'suvs'),
	(72, 'Mesa Trail', 'mesa3', 40000, 'suvs'),
	(73, 'Patriot', 'patriot', 55000, 'suvs'),
	(74, 'Radius', 'radi', 29000, 'suvs'),
	(75, 'Rocoto', 'rocoto', 45000, 'suvs'),
	(76, 'Seminole', 'seminole', 25000, 'suvs'),
	(77, 'XLS', 'xls', 32000, 'suvs'),
	(78, 'Btype', 'btype', 62000, 'sportsclassics'),
	(79, 'Btype Luxe', 'btype3', 85000, 'sportsclassics'),
	(80, 'Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
	(81, 'Casco', 'casco', 30000, 'sportsclassics'),
	(82, 'Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
	(83, 'Manana', 'manana', 12800, 'sportsclassics'),
	(84, 'Monroe', 'monroe', 55000, 'sportsclassics'),
	(85, 'Pigalle', 'pigalle', 20000, 'sportsclassics'),
	(86, 'Stinger', 'stinger', 80000, 'sportsclassics'),
	(87, 'Stinger GT', 'stingergt', 75000, 'sportsclassics'),
	(88, 'Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
	(89, 'Z-Type', 'ztype', 220000, 'sportsclassics'),
	(90, 'Bifta', 'bifta', 12000, 'offroad'),
	(91, 'Bf Injection', 'bfinjection', 16000, 'offroad'),
	(92, 'Blazer', 'blazer', 6500, 'offroad'),
	(93, 'Blazer Sport', 'blazer4', 8500, 'offroad'),
	(94, 'Brawler', 'brawler', 45000, 'offroad'),
	(95, 'Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
	(96, 'Dune Buggy', 'dune', 8000, 'offroad'),
	(97, 'Guardian', 'guardian', 45000, 'offroad'),
	(98, 'Rebel', 'rebel2', 35000, 'offroad'),
	(99, 'Sandking', 'sandking', 55000, 'offroad'),
	(100, 'The Liberator', 'monster', 210000, 'offroad'),
	(101, 'Trophy Truck', 'trophytruck', 60000, 'offroad'),
	(102, 'Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
	(103, 'Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
	(104, 'Exemplar', 'exemplar', 32000, 'coupes'),
	(105, 'F620', 'f620', 40000, 'coupes'),
	(106, 'Felon', 'felon', 42000, 'coupes'),
	(107, 'Felon GT', 'felon2', 55000, 'coupes'),
	(108, 'Jackal', 'jackal', 38000, 'coupes'),
	(109, 'Oracle XS', 'oracle2', 35000, 'coupes'),
	(110, 'Sentinel', 'sentinel', 32000, 'coupes'),
	(111, 'Sentinel XS', 'sentinel2', 40000, 'coupes'),
	(112, 'Windsor', 'windsor', 95000, 'coupes'),
	(113, 'Windsor Drop', 'windsor2', 125000, 'coupes'),
	(114, 'Zion', 'zion', 36000, 'coupes'),
	(115, 'Zion Cabrio', 'zion2', 45000, 'coupes'),
	(116, '9F', 'ninef', 65000, 'sports'),
	(117, '9F Cabrio', 'ninef2', 80000, 'sports'),
	(118, 'Alpha', 'alpha', 60000, 'sports'),
	(119, 'Banshee', 'banshee', 70000, 'sports'),
	(120, 'Bestia GTS', 'bestiagts', 55000, 'sports'),
	(121, 'Buffalo', 'buffalo', 12000, 'sports'),
	(122, 'Buffalo S', 'buffalo2', 20000, 'sports'),
	(123, 'Carbonizzare', 'carbonizzare', 75000, 'sports'),
	(124, 'Comet', 'comet2', 65000, 'sports'),
	(125, 'Coquette', 'coquette', 65000, 'sports'),
	(126, 'Drift Tampa', 'tampa2', 80000, 'sports'),
	(127, 'Elegy', 'elegy2', 38500, 'sports'),
	(128, 'Feltzer', 'feltzer2', 55000, 'sports'),
	(129, 'Furore GT', 'furoregt', 45000, 'sports'),
	(130, 'Fusilade', 'fusilade', 40000, 'sports'),
	(131, 'Jester', 'jester', 65000, 'sports'),
	(132, 'Jester(Racecar)', 'jester2', 135000, 'sports'),
	(133, 'Khamelion', 'khamelion', 38000, 'sports'),
	(134, 'Kuruma', 'kuruma', 30000, 'sports'),
	(135, 'Lynx', 'lynx', 40000, 'sports'),
	(136, 'Mamba', 'mamba', 70000, 'sports'),
	(137, 'Massacro', 'massacro', 65000, 'sports'),
	(138, 'Massacro(Racecar)', 'massacro2', 130000, 'sports'),
	(139, 'Omnis', 'omnis', 35000, 'sports'),
	(140, 'Penumbra', 'penumbra', 28000, 'sports'),
	(141, 'Rapid GT', 'rapidgt', 35000, 'sports'),
	(142, 'Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
	(143, 'Schafter V12', 'schafter3', 50000, 'sports'),
	(144, 'Seven 70', 'seven70', 39500, 'sports'),
	(145, 'Sultan', 'sultan', 15000, 'sports'),
	(146, 'Surano', 'surano', 50000, 'sports'),
	(147, 'Tropos', 'tropos', 40000, 'sports'),
	(148, 'Verlierer', 'verlierer2', 70000, 'sports'),
	(149, 'Adder', 'adder', 900000, 'super'),
	(150, 'Banshee 900R', 'banshee2', 255000, 'super'),
	(151, 'Bullet', 'bullet', 90000, 'super'),
	(152, 'Cheetah', 'cheetah', 375000, 'super'),
	(153, 'Entity XF', 'entityxf', 425000, 'super'),
	(154, 'ETR1', 'sheava', 220000, 'super'),
	(155, 'FMJ', 'fmj', 185000, 'super'),
	(156, 'Infernus', 'infernus', 180000, 'super'),
	(157, 'Osiris', 'osiris', 160000, 'super'),
	(158, 'Pfister', 'pfister811', 85000, 'super'),
	(159, 'RE-7B', 'le7b', 325000, 'super'),
	(160, 'Reaper', 'reaper', 150000, 'super'),
	(161, 'Sultan RS', 'sultanrs', 65000, 'super'),
	(162, 'T20', 't20', 300000, 'super'),
	(163, 'Turismo R', 'turismor', 350000, 'super'),
	(164, 'Tyrus', 'tyrus', 600000, 'super'),
	(165, 'Vacca', 'vacca', 120000, 'super'),
	(166, 'Voltic', 'voltic', 90000, 'super'),
	(167, 'X80 Proto', 'prototipo', 2500000, 'super'),
	(168, 'Zentorno', 'zentorno', 1500000, 'super'),
	(169, 'Akuma', 'AKUMA', 7500, 'motorcycles'),
	(170, 'Avarus', 'avarus', 18000, 'motorcycles'),
	(171, 'Bagger', 'bagger', 13500, 'motorcycles'),
	(172, 'Bati 801', 'bati', 12000, 'motorcycles'),
	(173, 'Bati 801RR', 'bati2', 19000, 'motorcycles'),
	(174, 'BF400', 'bf400', 6500, 'motorcycles'),
	(175, 'BMX (velo)', 'bmx', 160, 'motorcycles'),
	(176, 'Carbon RS', 'carbonrs', 18000, 'motorcycles'),
	(177, 'Chimera', 'chimera', 38000, 'motorcycles'),
	(178, 'Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
	(179, 'Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
	(180, 'Daemon', 'daemon', 11500, 'motorcycles'),
	(181, 'Daemon High', 'daemon2', 13500, 'motorcycles'),
	(182, 'Defiler', 'defiler', 9800, 'motorcycles'),
	(183, 'Double T', 'double', 28000, 'motorcycles'),
	(184, 'Enduro', 'enduro', 5500, 'motorcycles'),
	(185, 'Esskey', 'esskey', 4200, 'motorcycles'),
	(186, 'Faggio', 'faggio', 1900, 'motorcycles'),
	(187, 'Vespa', 'faggio2', 2800, 'motorcycles'),
	(188, 'Fixter (velo)', 'fixter', 225, 'motorcycles'),
	(189, 'Gargoyle', 'gargoyle', 16500, 'motorcycles'),
	(190, 'Hakuchou', 'hakuchou', 31000, 'motorcycles'),
	(191, 'Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
	(192, 'Hexer', 'hexer', 12000, 'motorcycles'),
	(193, 'Innovation', 'innovation', 23500, 'motorcycles'),
	(194, 'Manchez', 'manchez', 5300, 'motorcycles'),
	(195, 'Nemesis', 'nemesis', 5800, 'motorcycles'),
	(196, 'Nightblade', 'nightblade', 35000, 'motorcycles'),
	(197, 'PCJ-600', 'pcj', 6200, 'motorcycles'),
	(198, 'Ruffian', 'ruffian', 6800, 'motorcycles'),
	(199, 'Sanchez', 'sanchez', 5300, 'motorcycles'),
	(200, 'Sanchez Sport', 'sanchez2', 5300, 'motorcycles'),
	(201, 'Sanctus', 'sanctus', 25000, 'motorcycles'),
	(202, 'Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
	(203, 'Sovereign', 'sovereign', 22000, 'motorcycles'),
	(204, 'Shotaro Concept', 'shotaro', 320000, 'motorcycles'),
	(205, 'Thrust', 'thrust', 24000, 'motorcycles'),
	(206, 'Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
	(207, 'Vader', 'vader', 7200, 'motorcycles'),
	(208, 'Vortex', 'vortex', 9800, 'motorcycles'),
	(209, 'Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
	(210, 'Zombie', 'zombiea', 9500, 'motorcycles'),
	(211, 'Zombie Luxuary', 'zombieb', 12000, 'motorcycles');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicle_categories
CREATE TABLE IF NOT EXISTS `vehicle_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.vehicle_categories: ~11 rows (approximately)
DELETE FROM `vehicle_categories`;
/*!40000 ALTER TABLE `vehicle_categories` DISABLE KEYS */;
INSERT INTO `vehicle_categories` (`id`, `name`, `label`) VALUES
	(1, 'compacts', 'Compacts'),
	(2, 'coupes', 'Coupés'),
	(3, 'sedans', 'Sedans'),
	(4, 'sports', 'Sports'),
	(5, 'sportsclassics', 'Sports Classics'),
	(6, 'super', 'Super'),
	(7, 'muscle', 'Muscle'),
	(8, 'offroad', 'Off Road'),
	(9, 'suvs', 'SUVs'),
	(10, 'vans', 'Vans'),
	(11, 'motorcycles', 'Motos');
/*!40000 ALTER TABLE `vehicle_categories` ENABLE KEYS */;

-- Dumping structure for table essentialmode.weashops
CREATE TABLE IF NOT EXISTS `weashops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.weashops: ~40 rows (approximately)
DELETE FROM `weashops`;
/*!40000 ALTER TABLE `weashops` DISABLE KEYS */;
INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
	(1, 'GunShop', 'WEAPON_PISTOL', 300),
	(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
	(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
	(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
	(5, 'GunShop', 'WEAPON_MACHETE', 90),
	(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
	(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
	(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
	(9, 'GunShop', 'WEAPON_BAT', 100),
	(10, 'BlackWeashop', 'WEAPON_BAT', 100),
	(11, 'GunShop', 'WEAPON_STUNGUN', 50),
	(12, 'BlackWeashop', 'WEAPON_STUNGUN', 50),
	(13, 'GunShop', 'WEAPON_MICROSMG', 1400),
	(14, 'BlackWeashop', 'WEAPON_MICROSMG', 1700),
	(15, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3400),
	(16, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500),
	(17, 'GunShop', 'WEAPON_ASSAULTRIFLE', 10000),
	(18, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000),
	(19, 'GunShop', 'WEAPON_SPECIALCARBINE', 15000),
	(20, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500),
	(21, 'GunShop', 'WEAPON_SNIPERRIFLE', 22000),
	(22, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000),
	(23, 'GunShop', 'WEAPON_FIREWORK', 18000),
	(24, 'BlackWeashop', 'WEAPON_FIREWORK', 20000),
	(25, 'GunShop', 'WEAPON_GRENADE', 500),
	(26, 'BlackWeashop', 'WEAPON_GRENADE', 650),
	(27, 'GunShop', 'WEAPON_BZGAS', 200),
	(28, 'BlackWeashop', 'WEAPON_BZGAS', 350),
	(29, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
	(30, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
	(31, 'GunShop', 'WEAPON_BALL', 50),
	(32, 'BlackWeashop', 'WEAPON_BALL', 50),
	(33, 'GunShop', 'WEAPON_SMOKEGRENADE', 100),
	(34, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
	(35, 'BlackWeashop', 'WEAPON_APPISTOL', 1100),
	(36, 'BlackWeashop', 'WEAPON_CARBINERIFLE', 12000),
	(37, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000),
	(38, 'BlackWeashop', 'WEAPON_MINIGUN', 45000),
	(39, 'BlackWeashop', 'WEAPON_RAILGUN', 50000),
	(40, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500);
/*!40000 ALTER TABLE `weashops` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
