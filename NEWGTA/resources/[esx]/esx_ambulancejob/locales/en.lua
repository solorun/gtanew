Locales['en'] = {
  -- Cloakroom
  ['cloakroom'] = 'locker Room',
  ['ems_clothes_civil'] = 'civilian Clothes',
  ['ems_clothes_ems'] = 'eMS Clothes',
  -- Vehicles
  ['veh_menu'] = 'vehicle',
  ['veh_spawn'] = 'press ~INPUT_CONTEXT~ to get a Vehicle or Helicopter',
  ['store_veh'] = 'press ~INPUT_CONTEXT~ to store the vehicle',
  ['ambulance'] = 'ambulance',
  ['helicopter'] = 'helicopter',
  -- Action Menu
  ['hospital'] = 'hospital',
  ['revive_inprogress'] = 'revive in progress',
  ['revive_complete'] = 'you have been revived ',
  ['no_players'] = 'no players nearby',
  ['no_vehicles'] = 'no vehicles nearby',
  ['isdead'] = 'is dead',
  ['unconscious'] = 'is not unconscious',
  -- Boss Menu
  ['deposit_society'] = 'eMS Deposit',
  ['withdraw_society'] = 'eMS Withdraw',
  ['boss_actions'] = 'boss Actions',
  -- Misc
  ['invalid_amount'] = '~r~invalid amount',
  ['open_menu'] = 'press ~INPUT_CONTEXT~ to open the menu',
  ['deposit_amount'] = 'deposit Amount',
  ['money_withdraw'] = 'amount withdrawn',
  ['fast_travel'] = 'Press ~INPUT_CONTEXT~ To fast Travel.',
  ['open_pharmacy'] = 'Press ~INPUT_CONTEXT~ To Open the Pharmacy.',
  ['pharmacy_menu_title'] = 'Pharmacy',
  ['pharmacy_take'] = 'take',
  ['medikit'] = 'medikit',
  ['bandage'] = 'bandage',
  ['max_item'] = 'You Cant Carry Anymore.',
  -- F6 Menu
  ['ems_menu'] = 'eMS Menu',
  ['ems_menu_title'] = 'ambulance - EMS Menu',
  ['ems_menu_revive'] = 'revive Player',
  ['ems_menu_putincar'] = 'put in Vehicle',
  ['ems_menu_small'] = 'heal small wounds',
  ['ems_menu_big'] = 'treat serious injuries',
  -- Phone
  ['alert_ambulance'] = 'alert Ambulance',
  -- Death
  ['respawn_at_hospital'] = 'do you want to be transported to the hospital?',
  ['yes'] = 'yes',
  ['please_wait'] = 'please wait ~b~',
  ['minutes'] = ' minutes ',
  ['seconds_fine'] = ' seconds ~w~to respawn \nRespawn now for ~g~$',
  ['seconds'] = ' seconds ~w~to respawn.',
  ['press_respawn_fine'] = '~w~ [Press ~b~E~w~]',
  ['press_respawn'] = 'press [~b~E~w~] to respawn.',
  -- Revive
  ['revive_help'] = 'revive a player',
}
