Config              = {}
Config.MarkerType   = 1
Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 5.0, y = 5.0, z = 3.0}
Config.MarkerColor  = {r = 100, g = 204, b = 100}
Config.RequiredCopsCoke = 2
Config.RequiredCopsMeth = 2
Config.RequiredCopsWeed = 0
Config.RequiredCopsOpium = 3
Config.Locale = 'en'

Config.Zones = {
	CokeFarm = 		 {x=2448.9228515625,  y=-1836.8076171875, z=51.953701019287},
	CokeTreatment =  {x=-458.13967895508, y=-2278.6174316406, z=7.5158290863037},
	CokeResell = 	 {x=-1756.1984863281, y=427.31674194336,  z=126.68292999268},
	MethFarm = 		 {x=1525.298828125, 	 y=1710.0217285156,  z=109.00956726074},
	MethTreatment =  {x=-1001.4151611328, y=4848.0087890625,  z=274.00686645508},
	MethResell = 	 {x=-63.592178344727, y=-1224.0709228516, z=27.768648147583},
	WeedFarm = 		 {x=1609.125, 		 y=6663.5942382813,  z=20.961572647095},
	WeedTreatment =  {x=91.061386108398,  y=3750.0380859375,  z=39.77326965332},
	WeedResell = 	 {x=-54.249694824219, y=-1443.3666992188, z=31.068626403809},
	OpiumFarm = 	 {x=1972.784790039,	 y=3819.3999023438,  z=33.428722381592},
	OpiumTreatment = {x=971.86499023438,y=-2157.61328125, z=28.475107192994},
	OpiumResell = 	 {x=2331.0881347656,y=2570.2250976562,z=46.681819915772}
}
Config.Map = {
  {name="Coke Farm",       color=6, scale=0.8, id=403, x=2448.9228515625,  y=-1836.8076171875, z=51.953701019287},
  {name="Coke Treatment",  color=6, scale=0.8, id=403, x=-458.13967895508, y=-2278.6174316406, z=7.5158290863037},
  {name="Coke Sales",      color=6, scale=0.8, id=403, x=-1756.1984863281, y=427.31674194336,  z=126.68292999268},
  {name="Meth Farm",       color=6, scale=0.8, id=403, x=1525.298828125, 	 y=1710.0217285156,  z=109.00956726074},
  {name="Meth Treatment",  color=6, scale=0.8, id=403, x=-1001.4151611328, y=4848.0087890625,  z=274.00686645508},
  {name="Meth Sales",      color=6, scale=0.8, id=403, x=-63.592178344727, y=-1224.0709228516, z=27.768648147583},
  {name="Opium Farm",      color=6, scale=0.8, id=403, x=1972.784790039,	 y=3819.3999023438,  z=33.428722381592},
  {name="Opium Treatment", color=6, scale=0.8, id=403, x=-971.86499023438,y=-2157.61328125, z=28.475107192994},
  {name="Opium Sales",     color=6, scale=0.8, id=403, x=2331.0881347656,y=2570.2250976562,z=46.681819915772},
  {name="Weed Farm",       color=2, scale=0.8, id=140, x=1609.125, 		 y=6663.5942382813,  z=20.961572647095},
  {name="Weed Treatment",  color=2, scale=0.8, id=140, x=91.061386108398,  y=3750.0380859375,  z=39.77326965332},
  {name="Weed Sales",      color=2, scale=0.8, id=140, x=-54.249694824219, y=-1443.3666992188, z=31.068626403809},
}
